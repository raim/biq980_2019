from opentrons import labware, instruments, robot, modules, containers

metadata = {'protocolName':'SimplePipette',
            'author':'Eric Behle <eric.behle@hhu.de>',
            'description':'Example script, pipetting diagonally along a 48 well plate',
            'version':'1.0.0',
            'last_modified':'2019/11/18'}


#=========================== robot config ============================================

target_speeds = {'x': '600', 'y':'400', 'z': '125', 'a': '100'} # Set speed along each axis
robot.head_speed(**target_speeds, combined_speed=max(target_speeds.values()))

#================================ labware config =====================================

plate_name1 = 'biolector-flower-plate-48-well'
if plate_name1 not in labware.list():
    custom_plate = labware.create( # Design a custom plate
        plate_name1,                    # name of you labware
        grid=(8, 6),                    # specify amount of (columns, rows)
        spacing=(13, 13),               # distances (mm) between each (column, row)
        diameter=10.85,                     # diameter (mm) of each well on the plate
        depth=33,                       # depth (mm) of each well on the plate
        volume=3200)



tiprack = labware.load('opentrons-tiprack-300ul', '7')# Define tiprack type and position
plate = labware.load('biolector-flower-plate-48-well', '11')# Define target plate type and position
trash = robot.fixed_trash# Define trash type and position. Here, the robot's own trash is used
reservoir = labware.load('opentrons_10_tuberack_nest_4x50ml_6x15ml_conical', '10')# liquid source type and position

#=================================== pipette config ========================

p300 = instruments.P300_Single(tip_racks=[tiprack], 
                             mount="left")


#================================== Pipetting protocol ==============================


# Transfer 900 ul from the first well of the reservoir to each of the four wells specified in plate.wells();
# replace tip afterwards
p300.transfer(900, reservoir.wells('A1').top(-30),# lower pipette tip 30 mm below the top of the source labware
                 plate.wells('A1', 'D4', 'E8', 'F8'),
                     new_tip = 'once')