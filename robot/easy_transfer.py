
from opentrons import robot, labware, instruments, protocol_api
#from opentrons import 
#import numpy as np
#from copy import deepcopy






# metadata
metadata = {
    'protocolName': 'Transfer',
    'author': 'Tim Petzel',
    'description': '\
        Protocol designed to pipette a gradient along all wells of a 96 well plate'
}



#================================ Load labware ==================================

plate_name_96 = 'corning_96_wellplate_360ul_flat'
plate_name_48 = 'biolector-flower-plate-48-well'

if plate_name_48 not in labware.list():
    custom_plate = labware.create(
        plate_name_48,                    # name of labware
        grid=(8, 6),                    # amount of (columns, rows)
        spacing=(13, 13),               # distances (mm) between each (column, row)
        diameter=10.85,                     # diameter (mm) of each well on the plate
        depth=33,                       # depth (mm) of each well on the plate
        volume=3200)


tip_name = 'Biozym-tiprack-200ul' # Add Biozym 200 ul tiprack to labware library
if tip_name not in labware.list():
    custom_tip = labware.create(
        tip_name,                    # name of you labware
        grid=(12, 8),                    # specify amount of (columns, rows)
        spacing=(9, 9),               # distances (mm) between each (column, row)
        diameter=5.23,                     # diameter (mm) of each well on the plate
        depth=50,                       # depth (mm) of each well on the plate
        volume=200)

#plate = labware.load(plate_name, '7') # Load well plate
plate_96 = labware.load(plate_name_96, '8')
plate_48 = labware.load(plate_name_48, '7')

tiprack = labware.load(tip_name, '11')
#tiprack = labware.load('opentrons-tiprack-300ul', '11')# Load tiprack

# pipettes
pipette = instruments.P300_Single(mount='left', tip_racks=[tiprack], max_volume = 200) # Load pipette


for x in plate_48:
    
    pipette.transfer(150, plate_48.wells(x.get_name()), plate_96.wells(x.get_name()))

