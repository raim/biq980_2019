from opentrons import robot, labware, instruments, protocol_api
#from opentrons import 
#import numpy as np
#from copy import deepcopy






# metadata
metadata = {
    'protocolName': '96WellFullGradient',
    'author': 'Eric Behle eric.behle@hhu.de',
    'description': '\
        Protocol designed to pipette a gradient along all wells of a 96 well plate'
}

# =============================== Define custom functions =================================

def ericsVolumeGradient(vol1, vol2, wellNumber, pipette, source, destination, sourceHeight, destHeight):
    """ Creates a volume gradient with a choosable height of pipette above source and destination;
        This was written due to a flaw in the api command for gradient pipetting
    kwargs:
         vol1 --> volume at beginning of gradient (double)       
         vol2 --> volume at end of gradient (double)
         wellNumber --> number of wells included in gradient, i.e. number of wells in destination (int)
         pipette --> pipette used (opentrons.legacy_api.instruments.pipette.Pipette)
         source --> Source from which to pipette (opentrons.legacy_api.containers.placeable.Well)
         destination --> list of wells in which to pipette (opentrons.legacy_api.containers.placeable.WellSeries)
         sourceHeight --> Distance of pipette tip from BOTTOM of source well (int)
         destHeight --> Distance of pipette tip from BOTTOM of destination well (int)
    """
    minVol = 1 #Minimum volume to be pipetted
    if wellNumber == 1:
        pipette.transfer(abs(vol1-vol2),
                         source.bottom(sourceHeight),
                         destination.bottom(destHeight),
                         new_tip='never'
                         )
        return
    
    dV = -1 * (vol1-vol2)/(wellNumber-1)
    for i in range(wellNumber):
        if vol1+i*dV >= minVol:
            pipette.transfer(vol1 + i * dV,
                     source.bottom(sourceHeight),
                     destination(i).bottom(destHeight),
                     new_tip='never'
                     )
    return 


#================================ Load labware ==================================

plate_name = 'corning_96_wellplate_360ul_flat'


tip_name = 'Biozym-tiprack-200ul' # Add Biozym 200 ul tiprack to labware library
if tip_name not in labware.list():
    custom_tip = labware.create(
        tip_name,                    # name of you labware
        grid=(12, 8),                    # specify amount of (columns, rows)
        spacing=(9, 9),               # distances (mm) between each (column, row)
        diameter=5.23,                     # diameter (mm) of each well on the plate
        depth=50,                       # depth (mm) of each well on the plate
        volume=200)

#plate = labware.load(plate_name, '7') # Load well plate
plate = labware.load(plate_name, '7')

tiprack = labware.load(tip_name, '11')
#tiprack = labware.load('opentrons-tiprack-300ul', '11')# Load tiprack

trough = labware.load('opentrons_10_tuberack_falcon_4x50ml_6x15ml_conical', '10') # Load reservoir

# pipettes
pipette = instruments.P300_Single(mount='left', tip_racks=[tiprack], max_volume = 200) # Load pipette


#========================== Define parameters ==========================================

tipLength = 51 # Length of pipette tips in mm
liqDistTop = 20# Length of pipette tip which should remain above liquid
lowestHeight = 2.5# Lowest height on which to set the pipette tip in mm

Nrow = 8 # Number of rows
Ncolumn = 12 # Number of columns
Nwells = Nrow*Ncolumn
maxPerWell = 150# Total liquid volume inside each well
minInducer = 0# Minimal amount of acetate 
maxInducer = 135# Maximal amount of acetate



#====================================== Notes ============================================


#"culture", "inducer", "medium" fuer 96er

#============================= Set source locations ==================================

inducerSource = trough.wells('A3')# M9 medium with max concentration of glucose
mediumSource = trough.wells('A4')# M9 medium for dilution
cultureSource = trough.wells('A1')# M9 medium with bacteria (and glucose)

#========================== ADAPT THESE PARAMETERS FOR EACH RUN ========================
#========================== Define tip at which to start pipetting =====================

pipette.start_at_tip(tiprack['A1'])

#============================== Define height of liquid in source reservoirs (currently 50 ml falcons)

liqHeightCulture = 35 # mm
liqHeightMedium = 35 # mm
liqHeightInducer = 35 # mm


#============================= Define tip distances from bottom of plates ===================

falconHeight = 115 #Height of a falcon in mm
initHeight = falconHeight # initial height of pipette above reservoirs

tipDistCulture = initHeight # Initial distance of pipette tip to BOTTOM of falcon tube for acetate pre-culture [mm]
tipDistMedium = initHeight # Initial distance of pipette tip to BOTTOM of falcon tube for M9 [mm]
tipDistInducer = initHeight # Initial distance of pipette tip to BOTTOM of falcon tube for M9 with acetate [mm]



if liqHeightCulture - tipLength > 0:
    tipDistCulture = liqHeightCulture - tipLength + liqDistTop
else:
    tipDistCulture = lowestHeight

if liqHeightMedium - tipLength > 0:
    tipDistMedium = liqHeightMedium - tipLength + liqDistTop
else:
    tipDistMedium = lowestHeight
    
if liqHeightInducer - tipLength > 0:
    tipDistInducer = liqHeightInducer - tipLength + liqDistTop
else:
    tipDistInducer = lowestHeight


# In a falcon, 5000 ul of volume change correspond to a height change of roughly 9 mm
# --> Reduce the pipette height accordingly. It will be set to 10 mm per 5000 ul for convenience

volChangeFalcon = 5000 # 5000ul
distChangeFalcon = 10 # Distance change per volChangeFalcon (rough value)
rowHeightReduction =  0# Reduction of pipette tip height after each column. Updated depending on total volume pipetted
rowHeightReduction = 0 # Reduction of pipette tip height after each column. Updated depending on total volume pipetted





# In a falcon, 5000 ul of volume change correspond to a height change of roughly 9 mm
# --> Reduce the pipette height accordingly. It will be set to 10 mm per 5000 ul for convenience
distChangeFalcon = 10 # Distance change per volChangeFalcon (rough value)
volChangeFalcon = 5000 # 5000ul
columnHeightReduction =  0# Reduction of pipette tip height after each column. Updated depending on total volume pipetted
rowHeightReduction = 0 # Reduction of pipette tip height after each column. Updated depending on total volume pipetted

tipDistWellPlate = 10 # Distance of pipette tip to BOTTOM of flower plate; used to avoid cross-contamination

#========================== Distribute culture ==========================

source = cultureSource


pipette.distribute(maxPerWell - maxInducer,
                   source.bottom(tipDistCulture),
                   plate.wells()[3:-3])
#                   new_tip='never')
#pipette.drop_tip()# Drop current tip

#============================== Blanks ========================================

source = inducerSource

pipette.distribute(maxInducer,
                 source.bottom(tipDistInducer),
                 plate.wells()[:3])

source = mediumSource

pipette.distribute(maxPerWell - maxInducer,
                    source.bottom(tipDistMedium),
                    plate.wells()[:3])

pipette.distribute(maxPerWell,
                    source.bottom(tipDistMedium),
                        plate.wells()[-3:])



# ================== Gradients for first 7 columns =====================

source = mediumSource # Distribute M9 medium for dilution

#rowHeightReduction = distChangeFalcon * (sum(range(Ncolumn-2))*(maxGlucose-minGlucose)/(Ncolumn-2))/volChangeFalcon

pipette.pick_up_tip()

ericsVolumeGradient(minInducer,
                    maxInducer,
                    Nwells-6,
                    pipette,
                    source,
                    plate.wells()[3:-3],
                    tipDistMedium,
                    tipDistWellPlate
                    )
pipette.drop_tip()


source = inducerSource # Dilute glucose

pipette.pick_up_tip()
#    if tipDistGlucose - i * rowHeightReduction <= 0:
#        break;
ericsVolumeGradient(maxInducer,
                    minInducer,
                    Nwells-6,
                    pipette,
                    source,
                    plate.wells()[3:-3],
                    tipDistInducer,
                    tipDistWellPlate
                    )
pipette.drop_tip()


"""
Division by zero error when using built-in gradient method:
    What works: 
        transfer or distribute with specified volume and either destination.bottom() or no bottom()
        transfer with volume gradient and no bottom() for destination
    What does not work:
        transfer with volume gradient tuple and destination.bottom()
    Reason: 
        create_volume_gradient uses len(destination); len(destination) = Nwells,
        but len(destination.bottom(x)) = 2. This is because it is a list [destination, coordinates]
"""
