# Scripts

* 48 well plates for BioLector: `SeparateGradientsEricsGradient.py`
* 96 well plates for Clariostar: `96WellFullGradient.py`
* Transfer from 48 well plate to 96 well plate: `easy_transfer.py`

# How to write scripts

* Install opentrons library: requires python3, best via anaconda,
* Choose your weapon: install python API, eg. Spyder or Jupyter,
* Or try your luck with the online protocol designer:
  https://opentrons.com/protocols/designer/


## Install Opentrons API

`pip install --user opentrons`

## Simulate Your Protocol

```{python3}
from opentrons import simulate
protocol_file = open('/home/raim/work/hhu_talks/biq980/uebung_201912/biq980/opentrons/SimplePipette.py')
simulate.simulate(protocol_file)
```

or simply on command-line:

`opentrons_simulate easy_transfer.py`

